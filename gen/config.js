module.exports = {
    'MAIN_CLAIM': 'c1',
    'PLAN_INTERVAL': 10,
    'PLAN_FACTOR': {
        'scheduled_repairs': 2
    },
    'WORTHY': 10,
    'CLUSTER_WIDTH': 8,
    'CLUSTER_HEIGHT': 8,
    'SOURCE_STACK': 2,
    'PLOT_PRIORITY': [
        STRUCTURE_EXTENSION,
        STRUCTURE_CONTAINER,
        STRUCTURE_TOWER
    ],
    'REPAIR_PRIORITY': [
                            STRUCTURE_SPAWN,
                            STRUCTURE_CONTAINER,
                            STRUCTURE_POWER_SPAWN,
                            STRUCTURE_EXTENSION,
                            STRUCTURE_ROAD,
                            STRUCTURE_TOWER,
                            STRUCTURE_RAMPART,
                            STRUCTURE_WALL,
                            STRUCTURE_NUKER,
                            STRUCTURE_OBSERVER,
                            STRUCTURE_EXTRACTOR,
                            STRUCTURE_LAB,
                            STRUCTURE_TERMINAL,
                            STRUCTURE_STORAGE,
                            STRUCTURE_POWER_BANK,
                            STRUCTURE_LINK
                        ],
    'BUILDING_PRIORITY': [
                            STRUCTURE_SPAWN,
                            STRUCTURE_POWER_SPAWN,
                            STRUCTURE_TOWER,
                            STRUCTURE_NUKER,
                            STRUCTURE_OBSERVER,
                            STRUCTURE_EXTENSION,
                            STRUCTURE_EXTRACTOR,
                            STRUCTURE_CONTAINER,
                            STRUCTURE_ROAD,
                            STRUCTURE_LAB,
                            STRUCTURE_TERMINAL,
                            STRUCTURE_STORAGE,
                            STRUCTURE_POWER_BANK,
                            STRUCTURE_WALL,
                            STRUCTURE_RAMPART,
                            STRUCTURE_LINK
                        ]
};
