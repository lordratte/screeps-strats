var functions = require('tools.functions');

module.exports = {
    'evaluate_roads': function(creep) {
        var memory = Game.rooms[creep.pos.roomName].memory;
        memory.roads = memory.roads || {};
        var usage = memory.roads[[creep.pos.x, creep.pos.y].join()] || 0;
        if (usage < 255) {
            Game.rooms[creep.pos.roomName].memory.roads[[creep.pos.x, creep.pos.y].join()] = usage + 10;
        } else {
            Game.rooms[creep.pos.roomName].createConstructionSite(creep.pos, STRUCTURE_ROAD);
        }
    },
    'main': function(options) {
        var _ = require('lodash.4'); // Pollyfil
        var performance = performance==undefined ? {'now':()=>0} : performance;
        console.log(performance);
        Memory.roads = Memory.roads || {};
        var rooms = [];
        for (var s in Game.spawns) {
            rooms.push(Game.spawns[s].room);
        }
        var time_all = performance.now();
        for (var room of rooms) {
            Memory.rooms[room.name] = Memory.rooms[room.name] || {};
            Memory.rooms[room.name].harvest_points = Memory.rooms[room.name].harvest_points || [];
            var time_harvest_points = performance.now();
            Memory.rooms[room.name].harvest_points = _.unionWith(Memory.rooms[room.name].harvest_points, _.reduce(room.find(FIND_SOURCES), (tot, o)=>{
                var x,y;
                for (var dx in (x=[-1, 0, 1])) {
                    for (var dy in (y=[-1, 0, 1])) {
                        if (y[dy] == x[dx] && y[dy] == 0) {
                            continue;
                        }
                        try {
                            if (functions.isWalkable(room, o.pos.x+x[dx], o.pos.y+y[dy])) {
                                tot.push({
                                    'x': o.pos.x+x[dx],
                                    'y': o.pos.y+y[dy],
                                    'id': o.id,
                                    'creeps': [],
                                    'roomName': room.name
                                });
                            }
                        } catch (e){
                          Memory.debug && console.log(e);
                        }
                    }
                }
                return tot;
            }, []), (a, b)=>{return a.x==b.x&&a.y==b.y;});
            Memory.debug && console.log('Harvest points:\t'+(performance.now()-time_harvest_points));
            var time_trim_points = performance.now();
            Memory.rooms[room.name].harvest_points.forEach((o)=>{
                o.creeps = _.intersection(o.creeps, Object.keys(Game.creeps));
            });
            Memory.debug && console.log('Trim points:\t'+(performance.now()-time_trim_points));
            var time_scheduled_repairs = performance.now();
            if (!(Game.time%(Memory.config.PLAN_INTERVAL*Memory.config.PLAN_FACTOR.scheduled_repairs))) {
                Memory.rooms[room.name].scheduled_repairs = {};
                var scheduled_repairs = room.find(FIND_STRUCTURES, {
                    filter: function(object){
                        return (object.structureType === STRUCTURE_ROAD && (object.hits < object.hitsMax / 3))
                            || (object.structureType === STRUCTURE_RAMPART && (object.hits < object.hitsMax / 12));
                    }
                });
                for (var struct of scheduled_repairs) {
                    Memory.rooms[room.name].scheduled_repairs[struct.id] = true;
                }
            }
            Memory.debug && console.log('Scheduled repairs:\t'+(performance.now()-time_scheduled_repairs));


            /* Used for deciding if plots must be filled */
            var building_need = {};
            var need_plots = false;
            var time_need_plots = performance.now();
            for (var type of options.plot_order) {
                building_need[type] = CONTROLLER_STRUCTURES[type][room.controller.level]
                    - room.find(FIND_STRUCTURES, {filter: {'structureType': type}}).length;
                need_plots = (building_need[type] > 0) || need_plots;
            }
            Memory.debug && console.log('Need plots:\t'+(performance.now()-time_need_plots));
            //**************//
            var time_roads = performance.now();

            Memory.debug && console.log('Roads:\t'+(performance.now()-time_roads));
            var plots = [];
            var room_spawns = need_plots ? room.find(FIND_MY_SPAWNS) : [];
            var time_building_plots = performance.now();
            for (var room_spawn of room_spawns) {
                for (var x=-options.cluster_width;x<options.cluster_width;x++) {
                    for (var y=-options.cluster_height;y<options.cluster_height;y++) {
                        if ((x+y)%2==0) {
                            continue;
                        }
                        try {
                            if (functions.isWalkable(room, room_spawn.pos.x+x, room_spawn.pos.y+y)) {
                                plots.push({
                                    'x': room_spawn.pos.x+x,
                                    'y': room_spawn.pos.y+y
                                });
                            }
                        } catch (e) {}
                    }
                }
            }
            Memory.debug && console.log('Building plots:\t'+(performance.now()-time_building_plots));
            var tally = {};
            var time_build = performance.now();
            var build_O = 0;
            for (var type of options.plot_order) {
                var ex = false;
                for (var i=0; building_need[type] > i; i++) {
                    var plot = plots.pop();
                    if (plot === undefined) {
                        ex = true;
                        break;
                    }
                    var res = room.createConstructionSite(plot.x, plot.y, type);
                    tally[res] = tally[res] || 0;
                    tally[res]++;
                    build_O++;
                }
                if (ex) break;
            }
            Memory.debug && console.log('Build:\t'+(performance.now()-time_build));
            Memory.debug && console.log('(Build):\t'+build_O);
            Memory.debug && console.log(JSON.stringify(tally, null, 2));

            room.memory.exits = room.memory.exits || {};
            var time_walls = performance.now();
            var diff = {};
            diff[FIND_EXIT_TOP] = [0, 1];
            diff[FIND_EXIT_RIGHT] = [-1, 0];
            diff[FIND_EXIT_BOTTOM] = [0, -1];
            diff[FIND_EXIT_LEFT] = [1, 0];
            for (var side in diff) {
                var exits = _.map(room.find(parseInt(side)), (o)=> [o.x, o.y]);
                if (!exits.length) {
                  continue;
                }
                var [bx, by] = diff[side];
                var [ox, oy] = [bx*-1, by*-1];
                var [dx, dy] = [bx*2, by*2];
                var [px, py] = [+(by!=0), +(bx!=0)];

                var side_a = _.maxBy(exits, (o)=> px==1?o[0]:o[1]);
                var side_b = _.minBy(exits, (o)=> px==1?o[0]:o[1]);

                var close_a, close_b,
                    corner_a, corner_b,
                    tmp_len;

                tmp_len = exits.length;
                exits = _.unionWith([[side_a[0]+px, side_a[1]+py]], exits, _.isEqual);
                if (exits.length > tmp_len) {
                    close_a = [side_a[0]+(px*2)+ox, side_a[1]+(py*2)+oy];
                    corner_a = [side_a[0]+(px*2), side_a[1]+(py*2)];
                } else {
                    exits = _.unionWith([[side_a[0]-px, side_a[1]-py]], exits, _.isEqual);
                    close_a = [side_a[0]-(px*2)+ox, side_a[1]-(py*2)+oy];
                    corner_a = [side_a[0]-(px*2), side_a[1]-(py*2)];
                }

                tmp_len = exits.length;
                exits = _.unionWith([[side_b[0]+px, side_b[1]+py]], exits, _.isEqual);
                if (exits.length > tmp_len) {
                    close_b = [side_b[0]+(px*2)+ox, side_b[1]+(py*2)+oy];
                    corner_b = [side_b[0]+(px*2), side_b[1]+(py*2)];
                } else {
                    exits = _.unionWith([[side_b[0]-px, side_b[1]-py]], exits, _.isEqual);
                    close_b = [side_b[0]-(px*2)+ox, side_b[1]-(py*2)+oy];
                    corner_b = [side_b[0]-(px*2), side_b[1]-(py*2)];
                }
                exits = _.unionWith([close_b, close_a, corner_b, corner_a], exits, _.isEqual);
                var wall_locs = _.map(exits, (exit) => {
                    return {
                        'x': exit[0] + dx,
                        'y': exit[1] + dy
                    };
                });
                for (var loc of wall_locs) {
                    if (!room.memory.exits[side] && room.createConstructionSite(loc.x, loc.y, STRUCTURE_RAMPART) == OK) {
                        room.memory.exits[side] = loc;
                    } else if (room.memory.exits[side] && room.memory.exits[side].x==loc.x && room.memory.exits[side].y==loc.y) {
                        room.createConstructionSite(loc.x, loc.y, STRUCTURE_RAMPART);
                    } else {
                        room.createConstructionSite(loc.x, loc.y, STRUCTURE_WALL);
                    }
                }
            }
            Memory.debug && console.log('Walls:\t'+(performance.now()-time_walls));
        }
        console.log('All:\t'+(performance.now()-time_all));
    }
};
