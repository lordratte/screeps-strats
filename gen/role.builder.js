module.exports = {
    getPopulationQuota: function(){
        return this.population_quota;
    },
    population_quota: 3,
    idle_role: 'harvester',
    builds: [[WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE],
                [WORK,CARRY,MOVE]],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
	    if (creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
	    }
	    if (!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.building = true;
	    }

	    if (creep.memory.building) {
	        var targets = Game.rooms[creep.memory.home].find(FIND_CONSTRUCTION_SITES).concat(
                _.compact(
                    Object.keys(Game.flags).map((val)=>{return val.match(/^h[0-9]+/)? Game.flags[val].room : null})
                )
            );
	        var toRepair = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: function(object){
                    return (object.structureType === STRUCTURE_ROAD && (object.hits < object.hitsMax / 3))
                            || ((object.structureType === STRUCTURE_WALL
                            || object.structureType === STRUCTURE_CONTAINER) && (object.hits < object.hitsMax));
                }
            });

            if (targets.length) {
                if (creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if (toRepair) {
                if (creep.repair(toRepair) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(toRepair);
                }
            } else if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                creep.memory.building = false;
                return 1;
            } else { //Is idle and needs temporary job
                return 2;
            }
	    }
	    else {
	        roles['harvester'].do_harvest(creep);
	    }
	}
};
