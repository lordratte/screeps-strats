module.exports = {
    run: function(spawn) {
        var closest_renew = spawn.pos.findClosestByRange(FIND_MY_CREEPS, {
            'filter': (o) => !!o.memory.renew_me
        });
        spawn.renewCreep(closest_renew);
	}
};
