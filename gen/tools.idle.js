module.exports = {
    'moonlight' : function(creep, roles) {
        if (creep.memory.true_role != undefined) {
            console.log('Creep invalid idle\ntrue_role:'+creep.memory.true_role+'\nrole:'+creep.memory.role);
        } else if (creep.memory.role in roles) {
            if (Memory.debug) {
                creep.say('t: '+roles[creep.memory.role].idle_role);
            }
            creep.memory.true_role = creep.memory.role;
            creep.memory.role = roles[creep.memory.true_role].idle_role;
        }
    },
    'store_away' : function(creep, flag) {
        creep.moveTo(flag);
    }
}
