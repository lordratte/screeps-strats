module.exports = {
    getPopulationQuota: function(){
        return this.population_quota;
    },
    population_quota: 1,
    idle_role: 'harvester',
    builds: [[WORK,WORK,CARRY,CARRY,CARRY,MOVE,MOVE,MOVE],
                [WORK,CARRY,MOVE]],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
	    if (creep.carry.energy <= 0) {
            var target = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_CONTAINER && structure.store[RESOURCE_ENERGY] > 0);
                    }
            });
            if (target) {
                if (creep.withdraw(target, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(target);
                }
            } else {
                return 2;
            }
        } else {
            var targets = Game.rooms[creep.memory.home].find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return ((structure.structureType == STRUCTURE_EXTENSION ||
                                structure.structureType == STRUCTURE_SPAWN ||
                                structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity);
                    }
            });
            if (targets.length > 0) {
                if (creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                return 1;
            } else { //Is idle and needs temporary job
                return 2;
            }
        }
	}
};
