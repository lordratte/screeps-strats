/*
 * A set of functions to use in the console
 * for analytic purposes.
*/

//Get current tast distribution:
(()=>{
    var roles = {};
    for (var c in Game.creeps) {
        var creep = Game.creeps[c];
        if (creep.memory.role in roles) {
            roles[creep.memory.role]++;
        } else {
            roles[creep.memory.role] = 1;
        }
    }
    for (var r in roles) {
        var role_count = roles[r];
        console.log(r+': '+role_count);
    }
})();

//Set all creeps' home to their current room if it isn't already set:
(()=>{
    for (var c in Game.creeps) {
        var creep = Game.creeps[c];
        if (!creep.memory.home) {
            creep.memory.home = creep.room.name;
        }
    }
})();
