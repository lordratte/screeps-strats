var roles = {
    'archer'  : require('role.archer'),
    'harvester' : require('role.harvester'),
    'upgrader'  : require('role.upgrader'),
    'builder'   : require('role.builder'),
    'claimer'   : require('role.claimer'),
    'restorer'  : require('role.restorer')
};
var buildings = {
    'tower' : require('building.tower'),
    'spawn' : require('building.spawn')
};

var all = require('all_roles');

var tools_population = require('tools.population');
var tools_idle       = require('tools.idle');
var tools_city_planner = require('tools.city_planner');

var config = require('config');

module.exports.loop = function () {
    Memory.config = Memory.config || {};
    for (var key in config) {
        if (!(key in Memory.config)) {
            Memory.config[key] = config[key];
        }
    }
    Memory.energy_paches = Memory.energy_paches || {};
    for (var p in Memory.energy_paches) {
        if (Game.getObjectById(p)== null) {
            delete Memory.energy_paches[p];
        }
    }

    for (var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            var ind = _.findIndex(Memory.energy_paches, (o) => {
                return o == name;
            });
            delete Memory.energy_paches[ind];
            delete Memory.creeps[name];
        }
    }

    var interval = Game.time%Memory.config.PLAN_INTERVAL;
    Memory.debug && console.log(interval);
    if (!interval) {
        tools_city_planner.main({
            'cluster_height': Memory.config.CLUSTER_HEIGHT,
            'cluster_width': Memory.config.CLUSTER_WIDTH,
            'plot_order': Memory.config.PLOT_PRIORITY
        });
    }
    for (var s in Game.spawns) {
        tools_population(roles, Game.spawns[s]);
        buildings['spawn'].run(Game.spawns[s]);
    }
    for (var roomName in Game.rooms) {
        var towers = Game.rooms[roomName].find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
        for (var tower of towers) {
            try {
                buildings['tower'].run(tower);
            } catch (e) {
                console.log(e);
            }
        }
    }
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        tools_city_planner.evaluate_roads(creep);
        try {
            if (!all.recover(creep) && !all.restore(creep)) {
                switch(roles[creep.memory.role].run(creep, roles, {
                                                                    claim_flag: Memory.config.MAIN_CLAIM
                                                                  })) {
                    case 1:
                        if (roles[creep.memory.role].run(creep, roles, { claim_flag: Memory.config.MAIN_CLAIM}) === 2) {
                            var receptions = _.filter(Game.flags, (flag) => {
                                return flag.room == Game.rooms[creep.memory.home]
                                        && (flag.name.match(/^r[0-9]*$/i)||[]).length >0
                            });
                            if (receptions.length>0) {
                                tools_idle.store_away(creep, receptions[0]);
                            }
                        }
                        break;
                    case 2:
                        tools_idle.moonlight(creep, roles);
                        break;
                }
            }
        } catch(e){
            console.log(e.stack);
            //console.log(Object.keys(creep.roomName));
        }
    }
}
