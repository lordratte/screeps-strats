module.exports = {
    getPopulationQuota: function(){
        return this.population_quota;
    },
    population_quota:0,
    //'idle_role': undefined,
    builds: [[CLAIM, CLAIM,MOVE]],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
        if (options.claim_flag in Game.flags) {
            if (Game.flags[options.claim_flag].room != creep.room) {
                creep.moveTo(Game.flags[options.claim_flag]);
            } else {
                var controller = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (structure) => structure.structureType === STRUCTURE_CONTROLLER
                });
                switch (creep.claimController(controller)){
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(controller);
                        break;
                    case ERR_GCL_NOT_ENOUGH:
                        if (creep.reserveController(controller) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(controller);
                        }
                }
            }
        }
	}
};
