module.exports = {
    getPopulationQuota: function(){
        return this.population_quota;
    },
    population_quota: 3,
    idle_role: 'upgrader',
    builds: [[WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE],
                [WORK,CARRY,MOVE]],
    do_harvest: function(creep) {
        var droppedEnergy = creep.pos.findClosestByRange(FIND_DROPPED_ENERGY);
        if (droppedEnergy != null) {
            var pickupStatus = creep.pickup(droppedEnergy);
            if (pickupStatus == ERR_NOT_IN_RANGE) {
                creep.moveTo(droppedEnergy);
            }
        } else {
            var rooms = []
                        .concat(
                            [creep.room]
                        )
                        .concat(
                            [Game.rooms[creep.memory.home]]
                        )
                        .concat(
                            _.compact(
                                Object.keys(Game.flags).map((val)=>{return val.match(/^h[0-9]+/)? Game.flags[val].room : null})
                            )
                        );
            for (var r in rooms){
                var room = rooms[r];
                var sources = room.find(FIND_SOURCES);
                if (creep.memory.harvest_site == undefined || Game.getObjectById(creep.memory.harvest_site).room.name != creep.room.name) {
                    if (sources.length == 0) {
                        continue;
                    }
                    creep.memory.harvest_site = sources[0].id;
                }
                if (creep.harvest(Game.getObjectById(creep.memory.harvest_site)) == ERR_NOT_IN_RANGE) {
                    var i = 0;
                    while (creep.moveTo(Game.getObjectById(creep.memory.harvest_site))==ERR_NO_PATH) {
                        try {
                            creep.memory.harvest_site = sources[i++].id;
                        } catch (e) {
                            break;
                        }
                    }
                }
            }
        }
        if (Memory.debug) {
            creep.say(Game.getObjectById(creep.memory.harvest_site)||droppedEnergy);
        }
    },
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
	    if (creep.carry.energy < creep.carryCapacity) {
            this.do_harvest(creep);
        } else {
            var targets = Game.rooms[creep.memory.home].find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return ((structure.structureType == STRUCTURE_EXTENSION ||
                                structure.structureType == STRUCTURE_SPAWN ||
                                structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity) ||
                                (structure.structureType == STRUCTURE_CONTAINER && _.sum(structure.store) < structure.storeCapacity);
                    }
            });
            if (targets.length > 0) {
                if (targets[0].structureType == STRUCTURE_CONTAINER) {
                    if (targets[0].pos.isEqualTo(creep.pos)) {
                        creep.drop(RESOURCE_ENERGY, creep.carry[RESOURCE_ENERGY]);
                    } else {
                        creep.moveTo(targets[0]);
                    }
                } else if (creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                return 1;
            } else { //Is idle and needs temporary job
                return 2;
            }
        }
	}
};
