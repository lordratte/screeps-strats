#A Screeps AI package


####The two branches
* **gen** *[0.1.2]*: The stable branch of the AI. Gen being *general* because the strategy is standard and non-experimental. For the most part.
* **gen-dev** *[0.1.5]*: This is where the test code goes and where the `push.py` script pushes from by default.

More branches may be added but that will probably be to test new frameworks entirely.

####AI structure
* **Roles**: prefixed with `role.`, define the actions of creeps and all the metadata needed to handle the tasks they are designated to do.
* **Buildings**: prefixed with `building.` define the actions of buildings and related functions.

####TOFIX
1. ~~All harvesters go to get dropped energy~~

####TODO
1. Add archers, healers and mages.
2. Test out claiming

##Manual Interface

####Flags
**h**-flags indicate to the creeps that a room may be used to harvest from.
**c**-flags indicate that a room needs to be claimed. This includes building new structures.
