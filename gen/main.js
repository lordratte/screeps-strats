var roles = {
    'harvester' : require('role.harvester'),
    'upgrader'  : require('role.upgrader'),
    'builder'   : require('role.builder'),
    'claimer'   : require('role.claimer'),
    'restorer'  : require('role.restorer')
};
var buildings = {
    'tower' : require('building.tower')
};

var tools_population = require('tools.population');
var tools_idle       = require('tools.idle');
var config = require('config');

module.exports.loop = function () {
    for(var name in Memory.creeps) {
        if(!Game.creeps[name]) {
            delete Memory.creeps[name];
        }
    }

    for (var s in Game.spawns) {
        tools_population(roles, Game.spawns[s]);
    }
    for (var roomName in Game.rooms) {
        var towers = Game.rooms[roomName].find(FIND_MY_STRUCTURES, {filter: {structureType: STRUCTURE_TOWER}});
        for (var t in towers) {
            try {
                buildings['tower'].run(towers[t]);
            } catch (e) {
                console.log(e);
            }
        }
    }

    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        try {
            switch(roles[creep.memory.role].run(creep, roles, { claim_flag: config.MAIN_CLAIM})) {
                case 1:
                    if (roles[creep.memory.role].run(creep, roles, { claim_flag: config.MAIN_CLAIM}) === 2) {
                        var receptions = _.filter(Game.flags, (flag) => {
                            return flag.room == Game.rooms[creep.memory.home]
                                    && flag.name.match(/^r[0-9]*$/i).length >0
                        });
                        if (receptions.length>0) {
                            tools_idle.store_away(creep, receptions[0]);
                        }
                    }
                    break;
                case 2:
                    tools_idle.moonlight(creep, roles);
                    break;
            }
        } catch(e){
            console.log(e);
            //console.log(Object.keys(creep.roomName));
        }
    }
}
