var functions = require('tools.functions');
module.exports = function(roles, spawn) {
    var arr = functions.sort(roles, (a, b) => a.value.priority - b.value.priority);
    Memory.config.ROLE_COUNTS = Memory.config.ROLE_COUNTS || {};
    for (var a in arr) {
        if (arr[a].key in Memory.config.ROLE_COUNTS) {
            arr[a].value.population_quota = Memory.config.ROLE_COUNTS[arr[a].key];
        } else {
            Memory.config.ROLE_COUNTS[arr[a].key] = arr[a].value.population_quota;
        }
        var actual = _.filter(Game.creeps, (creep) => (creep.memory.true_role == arr[a].key
                                                    || (creep.memory.role == arr[a].key
                                                        && creep.memory.true_role == undefined)
                                                    ) && creep.memory.home === spawn.room.name);
        if (actual.length < arr[a].value.getPopulationQuota(spawn)) {
            var k = 1;
            while (k in Memory.creeps){k++;}
            for (var i in arr[a].value.builds) {
                var res = spawn.createCreep(arr[a].value.builds[i++], k, {role: arr[a].key, home: spawn.room.name});
                if (res === OK) {
                    console.log('Spawning new {1}: {2}'.replace('{1}', arr[a].key).replace('{2}',k));
                }
            }
        }
    }
}
