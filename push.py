import hashlib
import json
import glob
import os

import requests
with open('auth.txt', 'r') as f:
    text = f.read()
    auth = text.split('\n')[:2] #[username, password]
    auth += ['']*(2-len(auth))

def push(module, branch, ptr=False):
    URL = 'https://screeps.com/ptr/api/user/code' if ptr else 'https://screeps.com/api/user/code'
    j = {
        'branch': branch,
        'modules': {},
    }
    for fn in glob.glob(os.path.join(module, '*.js')):
        path, filename = os.path.split(fn)
        j['modules'][filename[:-3]] = open(fn).read()

    print j['modules'].keys()

    hashes = '{%s}' % ', '.join('%s: "%s"' % (k, hashlib.md5(v).hexdigest()) for k, v in j['modules'].iteritems())
    j['modules']['main'] = j['modules']['main'].replace("'__HASHES__'", hashes)

    r = requests.post(URL,
                      auth=auth, data=json.dumps(j),
                      headers={'Content-Type': 'application/json; charset=utf-8'})

    print r.content
    print r.status_code

if __name__ == '__main__':
    import sys
    auth[0] = raw_input('Username>') or auth[0]
    auth[1] = raw_input('Password>') or auth[1]
    auth = tuple(auth)
    push(raw_input('Module (gen)>') or 'gen', raw_input('Branch (gen-dev)>') or 'gen-dev', len(sys.argv) > 1)
