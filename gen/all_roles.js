var functions = require('tools.functions');
module.exports = {
	'recover' : function(creep){
		if (creep.room.name != creep.memory.home && !Game.spawns.length) {
			creep.moveTo(Game.rooms[creep.memory.home].controller);
			return true;
		}
	},
	'restore' : function(creep) {
		if (creep.ticksToLive > CREEP_LIFE_TIME*0.95) {
			delete creep.memory.renew_me;
			return false;
		}
		if (creep.body.length > Memory.config.WORTHY
						&& creep.ticksToLive < CREEP_LIFE_TIME*0.1
						|| creep.memory.renew_me) {
			creep.memory.renew_me = true;
			var [x2, y2] = functions.roomLoc(creep.pos.roomName);
			var spawns = functions.sort(Game.spawns, (a, b)=> {
				var [ax1, ay1] = functions.roomLoc(a.value.pos.roomName);
				var [bx1, by1] = functions.roomLoc(b.value.pos.roomName);
				var dist_a =  Math.sqrt(Math.pow(ax1-x2, 2)+Math.pow(ay1-y2, 2));
				var dist_b =  Math.sqrt(Math.pow(bx1-x2, 2)+Math.pow(by1-y2, 2));
				return dist_a - dist_b;
			});
			if (spawns.length) {
				creep.moveTo(spawns[0].value);
				return true;
			}
		}
	}
}
