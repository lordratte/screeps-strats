# A Screeps AI package

This package started off as a simple, personal project for easy committing to Screeps but after some time, it has become a slightly less simple, personal project for easy committing to Screeps.


#### The branch
* **gen** *[0.3.5]*: The 'stable' branch of the 'AI'. Gen being *general* because the strategy is standard and non-experimental. For the most part.

#### AI structure
* **Roles**: prefixed with `role.`, define the actions of creeps and all the metadata needed to handle the tasks they are designated to do.
* **Buildings**: prefixed with `building.` define the actions of buildings and related functions.

#### TOFIX
1. ~~All harvesters go to get dropped energy~~
2. ~~Balance load on sources~~
3. Use roads in rooms rather than rooms in roads - in memory

#### TODO
1. Add ~~archers~~, healers and mages.
2. Test out claiming
3. ~~Prioritize repairs~~
4. Prioritize restoring
5. Make creeps always be doing something
7. ~~If containers are half full (or some other fraction), creeps can take from those rather than harvest.~~
8. ~~Scale population with resource availability~~
9. Make repairing of ramparts more efficient - like repairing roads

## Manual Interface

For now, configuration must happen by directly modifying memory.

#### Flags
**h**-flags indicate to the creeps that a room may be used to harvest from.
**c**-flags indicate that a room needs to be claimed. This includes building new structures.
