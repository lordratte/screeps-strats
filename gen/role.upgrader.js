module.exports = {
    getPopulationQuota: function(spawn){
        spawn.room.memory.harvest_points = spawn.room.memory.harvest_points || [];
        var hapo = spawn.room.memory.harvest_points.length;
        return Math.round(this.population_quota * hapo / 4);
    },
    priority: 3,
    population_quota: 1,
    idle_role: 'builder',
    builds: [
                [MOVE,MOVE,MOVE,MOVE,WORK,CARRY,CARRY,CARRY,CARRY,CARRY],
                [WORK,CARRY,MOVE]
            ],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
        if (creep.memory.upgrading && creep.carry.energy == 0) {
            creep.memory.upgrading = false;
            if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                return 1;
            }

	    }
	    if (!creep.memory.upgrading && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.upgrading = true;
	    }

	    if (creep.memory.upgrading) {
            if (creep.upgradeController(Game.rooms[creep.memory.home].controller) == ERR_NOT_IN_RANGE) {
                creep.moveTo(Game.rooms[creep.memory.home].controller);
            }
        }
        else {
            roles['harvester'].do_harvest(creep, true);
        }
	}
};
