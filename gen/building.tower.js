module.exports = {

    /** @param {Tower} tower **/
    run: function(building) {

        var closestHostile = building.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if (closestHostile) {
            building.attack(closestHostile);
            return;
        }
        /*
        var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => structure.hits < structure.hitsMax
        });
        if (closestDamagedStructure) {
            tower.repair(closestDamagedStructure);
        }
        */

	}
};
