module.exports = {
    getPopulationQuota: function(){
        var flag_exists = false;
        for (var f in Game.flags) {
            if (f.match(/^c[0-9]+/)) {
                flag_exists = true;
            }
        }
        if (flag_exists) {
            return this.population_quota;
        } else {
            return 0;
        }
    },
    priority: 6,
    population_quota:1,
    //'idle_role': undefined,
    builds: [[CLAIM, CLAIM,MOVE]],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
        if (options.claim_flag in Game.flags) {
            if (Game.flags[options.claim_flag].room != creep.room) {
                creep.moveTo(Game.flags[options.claim_flag]);
            } else {
                var controller = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (structure) => structure.structureType === STRUCTURE_CONTROLLER
                });
                switch (creep.claimController(controller)){
                    case ERR_NOT_IN_RANGE:
                        creep.moveTo(controller);
                        break;
                    case ERR_GCL_NOT_ENOUGH:
                        if (creep.reserveController(controller) == ERR_NOT_IN_RANGE) {
                            creep.moveTo(controller);
                        }
                }
            }
        }
	}
};
