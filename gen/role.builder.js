var functions = require('tools.functions');
module.exports = {
    getPopulationQuota: function(spawn){
        spawn.room.memory.harvest_points = spawn.room.memory.harvest_points || [];
        var hapo = spawn.room.memory.harvest_points.length;
        return Math.round(this.population_quota * hapo / 4);
    },
    priority: 4,
    getRoomsNeeded: function(){
        var builders = _.filter(Game.creeps, (creep) => {
            return creep.memory.role == 'builder';
        });
        var builder_rooms = {};
        for (var builder of builders) {
            if (!(builder.room.name in builder_rooms)) {
                builder_rooms[builder.room.name] = 1;
            } else {
                builder_rooms[builder.room.name]++;
            }
        }
        var rooms = []
                    .concat(
                        _.compact(
                            Object.keys(Game.flags).map((val)=>{return val.match(/^c[0-9]+/)? Game.flags[val].room : null})
                        )
                    )
                    .concat(
                        _.values(Game.rooms)
                    );
        return _.filter(rooms, (room) => {
            return !(room.name in builder_rooms) || builder_rooms[room.name] <= 3; //TODO: Use the values of the amount of builders to decide better.
        });
    },
    population_quota: 3,
    idle_role: 'harvester',
    builds: [
                [MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
                [WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE],
                [WORK,CARRY,MOVE]],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
	    if (creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
	    }
	    if (!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.building = true;
	    }

	    if (creep.memory.building) {
            var targets = [];
            //console.log(this.getRoomsNeeded());
            var rooms = []
                        .concat(
                            [Game.rooms[creep.memory.home]]
                        )
                        .concat(
                            this.getRoomsNeeded()
                        );

            for (var room of rooms) {
    	        targets = targets.concat(room.find(FIND_CONSTRUCTION_SITES));
            }
            targets = targets.concat(
                _.compact(
                    Object.keys(Game.flags).map((val)=>{return val.match(/^h[0-9]+/)? Game.flags[val].room : null})
                )
            );
            creep.room.memory.scheduled_repairs = creep.room.memory.scheduled_repairs || {};
	        var to_repair = creep.room.find(FIND_STRUCTURES, {
                filter: function(object){
                    return ((object.structureType === STRUCTURE_ROAD
                            || object.structureType === STRUCTURE_RAMPART
                                    && creep.room.memory.scheduled_repairs[object.id] === true)
                            || object.structureType === STRUCTURE_WALL
                            || object.structureType === STRUCTURE_CONTAINER
                            || object.structureType === STRUCTURE_STORAGE) && (object.hits < object.hitsMax);
                }
            });

            if (targets.length) {
                targets = targets.sort((a, b)=> {
                    var type_pri = Memory.config.BUILDING_PRIORITY.indexOf(a.structureType) - Memory.config.BUILDING_PRIORITY.indexOf(b.structureType);
                    if (type_pri != 0) {
                        return type_pri;
                    } else {
                        return a.pos.getRangeTo(creep) - b.pos.getRangeTo(creep);
                    }
                });
                if (creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if (to_repair.length) {
                to_repair = to_repair.sort((a, b)=> {
                    var type_pri = Memory.config.REPAIR_PRIORITY.indexOf(a.structureType) - Memory.config.REPAIR_PRIORITY.indexOf(b.structureType);
                    if (type_pri != 0) {
                        return type_pri;
                    } else if (a.structureType===STRUCTURE_WALL) {
                        if (Math.abs(a.hits - b.hits) > 50) {
                            return a.hits - b.hits;
                        }
                    }
                    return a.pos.getRangeTo(creep) - b.pos.getRangeTo(creep);
                });
                var fix = to_repair[0];
                if (creep.repair(fix) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(fix);
                }
            } else if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                creep.memory.building = false;
                return 1;
            } else { //Is idle and needs temporary job
                return 2;
            }
	    }
	    else {
	        roles['harvester'].do_harvest(creep, true);
	    }
	}
};
