var functions = require('tools.functions');

module.exports = {
    getPopulationQuota: function(spawn){
        spawn.room.memory.harvest_points = spawn.room.memory.harvest_points || [];
        var hapo = spawn.room.memory.harvest_points.length;
        return Math.round(this.population_quota * hapo / 4);
    },
    priority: 2,
    population_quota: 3,
    idle_role: 'upgrader',
    builds: [
                [MOVE,MOVE,MOVE,MOVE,MOVE,WORK,WORK,WORK,WORK,WORK,WORK,WORK,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY,CARRY],
                [WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE],
                [WORK,CARRY,MOVE]],
    do_harvest: function(creep, can_withdraw) {
        var container;
        var droppedEnergy = creep.pos.findClosestByRange(FIND_DROPPED_RESOURCES);
        if (droppedEnergy!= null && Memory.energy_paches[droppedEnergy.id] === undefined) {
            Memory.energy_paches[droppedEnergy.id] = creep.name;
        }
        if (droppedEnergy!= null && Memory.energy_paches[droppedEnergy.id] == creep.name) {
            if (creep.pickup(droppedEnergy) == ERR_NOT_IN_RANGE) {
                creep.moveTo(droppedEnergy);
            }
        } else if ((container = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return (structure.structureType == STRUCTURE_CONTAINER
                            && structure.store[RESOURCE_ENERGY] > (structure.storeCapacity/2));
                    }
                })) != null && can_withdraw) {
            if (creep.withdraw(container, RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                creep.moveTo(container);
            }
        } else {
            var harvest_site
            var x1, y1;
            var menu = {};
            [x1, y1] = functions.roomLoc(creep.pos.roomName);
            for (var r in Memory.rooms){
                var x2, y2;
                var room = Memory.rooms[r];
                [x2, y2] = functions.roomLoc(r);
                var distance = Math.sqrt(Math.pow(x1-x2, 2)+Math.pow(y1-y2, 2));
                var owns = _.filter(room.harvest_points, (o)=>{
                    return o.creeps.indexOf(creep.name) > -1;
                }).pop();
                if (owns != null) {
                    harvest_site = owns;
                    break;
                }
                var points = _.filter(room.harvest_points, (o)=>{
                    return (o.creeps.length||0) < Memory.config.SOURCE_STACK;
                });
                if (points.length > 0) {
                    menu[distance] = points;
                }

            }
            if (harvest_site === undefined) {
                var opts = functions.sort(menu, (obj_a, obj_b) => {
                    return obj_a.key - obj_b.key;
                });
                var opt = opts.reverse().pop();
                if (opt != null) {
                    var closest_room = opt.value;
                    harvest_site = closest_room[0];
                    closest_room[0].creeps = _.union(closest_room[0].creeps, [creep.name]);
                }
            }
            var source = Game.getObjectById(harvest_site.id);
            if (source == null) {
                return;
            }
            if (creep.harvest(source) == ERR_NOT_IN_RANGE) {
                var res = creep.moveTo(new RoomPosition(harvest_site.x, harvest_site.y, harvest_site.roomName));
                // if (res == ERR_NO_PATH) {
                //     ...
                // }
            }
        }
        if (Memory.debug) {
            var pos = (source||droppedEnergy||container||{pos:{x:null,y:null}}).pos;
            creep.say(JSON.stringify([pos.x, pos.y]));
        }
    },
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
	    if (creep.carry.energy < creep.carryCapacity) {
            this.do_harvest(creep, false);
        } else {
            var targets = Game.rooms[creep.memory.home].find(FIND_STRUCTURES, {
                    filter: (structure) => {
                        return ((structure.structureType == STRUCTURE_EXTENSION ||
                                structure.structureType == STRUCTURE_SPAWN ||
                                structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity) ||
                                (structure.structureType == STRUCTURE_CONTAINER && _.sum(structure.store) < structure.storeCapacity);
                    }
            });
            if (targets.length > 0) {
                if (targets[0].structureType == STRUCTURE_CONTAINER) {
                    if (targets[0].pos.isEqualTo(creep.pos)) {
                        creep.drop(RESOURCE_ENERGY, creep.carry[RESOURCE_ENERGY]);
                    } else {
                        creep.moveTo(targets[0]);
                    }
                } else if (creep.transfer(targets[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                return 1;
            } else { //Is idle and needs temporary job
                return 2;
            }
        }
	}
};
