module.exports = function(roles, spawn) {

    for (var a in roles) {
        var actual = _.filter(Game.creeps, (creep) => (creep.memory.true_role == a
                                                    || (creep.memory.role == a
                                                        && creep.memory.true_role == undefined)
                                                    ) && creep.room === spawn.room);
        if (actual.length < roles[a].getPopulationQuota()) {
            var k = 1;
            while (k in Memory.creeps){k++;}
            for (var i in roles[a].builds) {
                var res = spawn.createCreep(roles[a].builds[i++], k, {role: a, home: spawn.room.name});
                if (res === OK) {
                    console.log('Spawning new {1}: {2}'.replace('{1}', a).replace('{2}',k));
                }
            }
        }
    }
}
