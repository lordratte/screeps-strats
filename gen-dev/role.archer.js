module.exports = {
    getPopulationQuota: function(){
        return this.population_quota;
    },
    population_quota: 0,
    //idle_role: '',
    builds: [
        //[RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE, MOVE, MOVE],
        //[RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE, MOVE],
        [RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE],
		[RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE]
    ],
    getRangedTarget: function()
	{
		var closeArchers = creep.pos.findNearest(HOSTILE_CREEPS, {
			filter: function(enemy)
			{
				return enemy.getActiveBodyparts(RANGED_ATTACK) > 0
					&& creep.pos.inRangeTo(enemy, 3);
			}
		});

		if(closeArchers != null)
			return closeArchers;

		var closeMobileMelee = creep.pos.findNearest(HOSTILE_CREEPS, {
			filter: function(enemy)
			{
				return enemy.getActiveBodyparts(ATTACK) > 0
					&& enemy.getActiveBodyparts(MOVE) > 0
					&& creep.pos.inRangeTo(enemy, 3);
			}
		});

		if(closeMobileMelee != null)
			return closeMobileMelee;

		var closeHealer = creep.pos.findNearest(HOSTILE_CREEPS, {
			filter: function(enemy)
			{
				return enemy.getActiveBodyparts(HEAL) > 0
					&& enemy.getActiveBodyparts(MOVE) > 0
					&& creep.pos.inRangeTo(enemy, 3);
			}
		});

		if(closeHealer != null)
			return closeHealer;

		return creep.pos.findNearest(HOSTILE_CREEPS);
	},
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
        var target = this.getRangedTarget();
		if(target !== null) {
            creep.rangedAttack(target);
        }
	}
};
