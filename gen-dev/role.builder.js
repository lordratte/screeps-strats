module.exports = {
    getPopulationQuota: function(){
        return this.population_quota;
    },
    getRoomsNeeded: function(){
        var builders = _.filter(Game.creeps, (creep) => {
            return creep.memory.role == 'builder';
        });
        var builder_rooms = {};
        for (var b in builders) {
            if (!(builders[b].room.name in builder_rooms)) {
                builder_rooms[builders[b].room.name] = 1;
            } else {
                builder_rooms[builders[b].room.name]++;
            }
        }
        var rooms = []
                    .concat(
                        _.compact(
                            Object.keys(Game.flags).map((val)=>{return val.match(/^c[0-9]+/)? Game.flags[val].room : null})
                        )
                    )
                    .concat(
                        _.values(Game.rooms)
                    );
        return _.filter(rooms, (room) => {
            return !(room.name in builder_rooms) || builder_rooms[room.name] <= 3; //TODO: Use the values of the amount of builders to decide better.
        });
    },
    population_quota: 3,
    idle_role: 'harvester',
    builds: [[WORK,WORK,WORK,CARRY,CARRY,MOVE,MOVE,MOVE],
                [WORK,CARRY,MOVE]],
    /** @param {Creep} creep **/
    run: function(creep, roles, options) {
	    if (creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
	    }
	    if (!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
	        creep.memory.building = true;
	    }

	    if (creep.memory.building) {
            var targets = [];
            //console.log(this.getRoomsNeeded());
            var rooms = []
                        .concat(
                            [Game.rooms[creep.memory.home]]
                        )
                        .concat(
                            this.getRoomsNeeded()
                        );

            for (var r in rooms) {
    	        targets = targets.concat(rooms[r].find(FIND_CONSTRUCTION_SITES));
            }
            targets = targets.concat(
                _.compact(
                    Object.keys(Game.flags).map((val)=>{return val.match(/^h[0-9]+/)? Game.flags[val].room : null})
                )
            );
	        var toRepair = creep.pos.findClosestByPath(FIND_STRUCTURES, {
                filter: function(object){
                    return (object.structureType === STRUCTURE_ROAD && (object.hits < object.hitsMax / 3))
                            || ((object.structureType === STRUCTURE_WALL
                            || object.structureType === STRUCTURE_CONTAINER) && (object.hits < object.hitsMax));
                }
            });

            if (targets.length) {
                if (creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0]);
                }
            } else if (toRepair) {
                if (creep.repair(toRepair) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(toRepair);
                }
            } else if (creep.memory.true_role in roles) { // Is idle and can revert to true task
                creep.memory.role = creep.memory.true_role;
                if (Memory.debug) {
                    creep.say('s: '+creep.memory.role);
                }
                creep.memory.true_role = undefined;
                creep.memory.building = false;
                return 1;
            } else { //Is idle and needs temporary job
                return 2;
            }
	    }
	    else {
	        roles['harvester'].do_harvest(creep);
	    }
	}
};
