module.exports = {
    getPopulationQuota: function(spawn){
        return Math.ceil(spawn.room.find(FIND_HOSTILE_CREEPS).length * 1.5);
        return 0;
    },
    priority: 1,
    population_quota: 0,
    //idle_role: '',
    builds: [
        //[RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE, MOVE, MOVE],
        //[RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE, MOVE],
        [RANGED_ATTACK, RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE, MOVE],
		[RANGED_ATTACK, RANGED_ATTACK, MOVE, MOVE],
        [TOUGH,MOVE,MOVE,RANGED_ATTACK]
    ],
    getRangedTarget: function(creep)
	{
		var closeArchers = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
			filter: function(enemy)
			{
				return enemy.getActiveBodyparts(RANGED_ATTACK) > 0
					&& creep.pos.inRangeTo(enemy, 3);
			}
		});

		if(closeArchers != null)
			return closeArchers;

		var closeMobileMelee = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
			filter: function(enemy)
			{
				return enemy.getActiveBodyparts(ATTACK) > 0
					&& enemy.getActiveBodyparts(MOVE) > 0
					&& creep.pos.inRangeTo(enemy, 3);
			}
		});

		if(closeMobileMelee != null)
			return closeMobileMelee;

		var closeHealer = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS, {
			filter: function(enemy)
			{
				return enemy.getActiveBodyparts(HEAL) > 0
					&& enemy.getActiveBodyparts(MOVE) > 0
					&& creep.pos.inRangeTo(enemy, 3);
			}
		});
		if(closeHealer != null)
			return closeHealer;

		return creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
	},
    run: function(creep, roles, options) {
        var target = this.getRangedTarget(creep);
		if(target !== null) {
            if (creep.rangedAttack(target) == ERR_NOT_IN_RANGE) {
                creep.moveTo(target);
            }
        }
	}
};
