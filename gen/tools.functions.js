module.exports = {
    'sort' : function (object, func) {
        var array = Object.keys(object).map((key) => ({
                                                'value': object[key],
                                                'key': key
                                            }));
        return array.sort(func);
    },
    'isWalkable' : function (room, x, y) {
        var orig = room.lookAt(x, y);
        var look_res = _.filter(orig, (v)=> {
            if (v.type == LOOK_TERRAIN) {
                return v.terrain == 'plain' || v.terrain == 'swamp';
            } else if (v.type == LOOK_STRUCTURES) {
                return !OBSTACLE_OBJECT_TYPES.concat([STRUCTURE_ROAD]).includes(v.structure.structureType);
            } else {
                return true;
            }
        });
        return orig.length == look_res.length;
    },
    'roomLoc' : (name) => {
        var spl = name.match(/[NSEW]|[0-9]+/g)
        if (spl && spl.length == 4) {
            return [parseInt(spl[1])*('E'==spl[0]? 1:-1), parseInt(spl[3])*('N'==spl[2]? 1:-1)];
        } else {
            return [0, 0];
        }
    }

};
